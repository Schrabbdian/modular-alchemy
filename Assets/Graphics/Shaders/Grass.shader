﻿
Shader "Custom/Grass"
{
	Properties
	{
		_Color("Tint Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_SpriteTex("Base (RGB)", 2D) = "white" {}
		_Shininess("Shininess", Float) = 2.5
		_SpecularStrength("Specular Strength", Range(0, 3)) = 1

		_GrassHeight("Grass Height", Range(0, 3)) = 0.5
		_GrassWidth("Grass Width", Range(0, 3)) = 0.5

		_WindSpeed("Wind Speed", Float) = 100
		_WindStength("Wind Strength", Float) = 0.05

		_BendAwayStrength("Bend Away Strength", Float) = 2.5
		_TrampleStrength("Trample Strength", Float) = 1.5
	}
	SubShader
	{
		
		Tags{ "RenderType" = "TransparentCutout" "Queue" = "AlphaTest" "LightMode" = "ForwardBase" }
		LOD 100
		Cull Off

		Pass
		{
			CGPROGRAM
			#pragma vertex VS_Main
			#pragma fragment FS_Main
			#pragma geometry GS_Main
			// compile shader into multiple variants, with and without shadows
			// (we don't care about any lightmaps yet, so skip these variants)
			#pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
			
			// shadow helper functions and macros
			#include "AutoLight.cginc"
			#include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc" // for _LightColor0

			// **************************************************************
			// Data structures												*
			// **************************************************************
			struct GS_INPUT
			{
				float4	pos			: POSITION;
				float3	normal		: NORMAL;
				float2  tex0		: TEXCOORD0;
			};

			struct FS_INPUT
			{
				float4	pos			: POSITION;
				float3 normal		: NORMAL;
				float2  tex0		: TEXCOORD0;
				float4	posWorld	: TEXCOORD1;
				SHADOW_COORDS(2) // put shadows data into TEXCOORD2
			};

			// **************************************************************
			// Vars															*
			// **************************************************************

			fixed4 _Color;

			float _GrassHeight;
			float _GrassWidth;
			float _Shininess;
			float _SpecularStrength;

			half _WindStength;
			half _WindSpeed;

			float4 _GrassObstaclePosition[64];
			float _GrassObstacleRadius[64];

			half _BendAwayStrength;
			half _TrampleStrength;

			Texture2D _SpriteTex;
			SamplerState sampler_SpriteTex;


			// **************************************************************
			// Shader Programs												*
			// **************************************************************

			// Vertex Shader ------------------------------------------------
			GS_INPUT VS_Main(appdata_base v)
			{
				GS_INPUT output = (GS_INPUT)0;

				output.pos = mul(unity_ObjectToWorld, v.vertex);
				output.normal = UnityObjectToWorldNormal(v.normal);
				output.tex0 = float2(0, 0);

				return output;
			}



			// Geometry Shader -----------------------------------------------------
			[maxvertexcount(4)]
			void GS_Main(point GS_INPUT p[1], inout TriangleStream<FS_INPUT> triStream)
			{
				
				//Calculate Billboard vectors
				float3 look = _WorldSpaceCameraPos - p[0].pos;
				look.y = 0;
				look = normalize(look);
				float3 up = p[0].normal;
				up = normalize(up - look * 2.5);// tilt grass tip so that it faces look direction more
				float3 right = normalize(cross(up, look));

				float3 root = p[0].pos;
				float3 tip = root + up * _GrassHeight;

				

				
				//Wind / Swaying
				float timeSpeed = _Time.x * _WindSpeed;
				float3 wind = float3(sin(timeSpeed + root.x) + sin(timeSpeed + root.z * 2) + sin(timeSpeed * 0.1 + root.x), 0, cos(timeSpeed + root.x * 2) + cos(timeSpeed + root.z));
				tip += wind * _WindStength;

				//Bending on Touch
				for (int i = 0; i < 64; i++)
				{
					if (_GrassObstacleRadius[i] > 0.0)
					{
						float3 d = root - _GrassObstaclePosition[i].xyz;
						float distance = sqrt(dot(d, d));

						float distLimit = _GrassObstacleRadius[i];// how far away does obstacle reach
						float distMulti = (distLimit - min(distLimit, distance * distance)) / distLimit; //distance falloff

						tip += normalize(d)*distMulti * _BendAwayStrength;//make grass bend away
						tip -= normalize(tip - root) * distMulti * _TrampleStrength;//flatten grass when on top of it
					}
				}

				if (sqrt(dot(tip - root, tip - root)) > _GrassHeight) { tip = normalize(tip - root) * _GrassHeight + root; }//ensure grass does not get longer

				//Generate Billboard Vertices
				float halfS = 0.5f * _GrassWidth;
				
				/*
				v3 v1
				v2 v0
				*/
				float4 v[4];
				v[0] = float4(root + halfS * right, 1.0f);
				v[1] = float4(tip + halfS * right, 1.0f);
				v[2] = float4(root - halfS * right, 1.0f);
				v[3] = float4(tip - halfS * right, 1.0f);

				//float3 faceNormal = normalize(cross(v[0] - v[2], v[3] - v[2]));
				//float3 faceNormal = normalize(tip - root);
				float3 faceNormal = p[0].normal;//this normal is used for lighting

				FS_INPUT pIn;
				pIn.pos = UnityObjectToClipPos(mul(unity_WorldToObject, v[0]));
				pIn.tex0 = float2(1.0f, 0.0f);
				pIn.normal = faceNormal;
				pIn.posWorld = v[0];
				TRANSFER_SHADOW(pIn);//receive shadows
				triStream.Append(pIn);

				pIn.pos = UnityObjectToClipPos(mul(unity_WorldToObject, v[1]));
				pIn.tex0 = float2(1.0f, 1.0f);
				pIn.normal = faceNormal;
				pIn.posWorld = v[1];
				TRANSFER_SHADOW(pIn);//receive shadows
				triStream.Append(pIn);

				pIn.pos = UnityObjectToClipPos(mul(unity_WorldToObject, v[2]));
				pIn.tex0 = float2(0.0f, 0.0f);
				pIn.normal = faceNormal;
				pIn.posWorld = v[2];
				TRANSFER_SHADOW(pIn);//receive shadows
				triStream.Append(pIn);

				pIn.pos = UnityObjectToClipPos(mul(unity_WorldToObject, v[3]));
				pIn.tex0 = float2(0.0f, 1.0f);
				pIn.normal = faceNormal;
				pIn.posWorld = v[3];
				TRANSFER_SHADOW(pIn);//receive shadows
				triStream.Append(pIn);
			}



			// Fragment Shader -----------------------------------------------
			float4 FS_Main(FS_INPUT input) : COLOR
			{
				float4 color = _SpriteTex.Sample(sampler_SpriteTex, input.tex0);
				clip(color.a - 0.05);//discard pixels with less than 0.05 alpha

				color *= _Color;

				//Base Lighting
				float3 l = _WorldSpaceLightPos0.xyz;
				float3 n = input.normal;
				float3 v = normalize(_WorldSpaceCameraPos - input.posWorld.xyz);//view Dir
				float3 r = normalize(reflect(-l, n));//reflection vector


				half nDotL = max(0, dot(n, l));
				fixed4 diffuse = nDotL * _LightColor0;//Diffuse
				fixed3 ambient = ShadeSH9(half4(input.normal, 1));//ambient
				

				float3 specular = _LightColor0.rgb * pow(max(0.0, dot(v, r)), _Shininess);

				specular *= input.tex0.y;//make specular intensity dependent on how close to the tip

				fixed shadow = SHADOW_ATTENUATION(input);

				return color * (shadow * diffuse + float4(ambient, 0)) + float4(_SpecularStrength * specular, 1);
			}

			ENDCG
		}
	}
}
