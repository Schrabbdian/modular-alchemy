﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateX_Test : MonoBehaviour {

    public float Speed = 5.0f;
	
	void Update () 
    {
        this.transform.Rotate(Vector3.right, Speed * Time.deltaTime);
	}
}
