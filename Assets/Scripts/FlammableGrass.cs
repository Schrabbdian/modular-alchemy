﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class FlammableGrass : Flammable
{
    [Header("Grass Specific:")]
    public Gradient colorByBurnTime;
    public float timeToBurn = 5.0f;

    private Renderer render;

    protected override void Start()
    {
        base.Start();

        render = GetComponent<Renderer>();
    }

    protected override void Update()
    {
        base.Update();

        if (IsBurning())
        {
            //make grass change color as it burns
            render.material.color = Color.Lerp(render.material.color, colorByBurnTime.Evaluate(burnTimer / timeToBurn), Time.deltaTime);
        }
    }
}
