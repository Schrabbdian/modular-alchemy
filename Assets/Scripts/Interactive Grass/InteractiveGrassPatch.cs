﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InteractiveGrassPatch : MonoBehaviour {

    public Vector3 grassBounds;
    public Vector3 grassBoundsOffset;
    [SerializeField] private MeshFilter grassMeshFilter;

    void Start()
    {
        grassMeshFilter.mesh.bounds = new Bounds(Vector3.up * grassBounds.y * 0.5f + grassBoundsOffset, grassBounds);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawWireCube(Vector3.up * grassBounds.y * 0.5f + grassBoundsOffset, grassBounds);
    }
}
