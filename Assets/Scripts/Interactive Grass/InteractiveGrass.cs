﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InteractiveGrass : MonoBehaviour 
{
    void LateUpdate()
    {
        SetObstaclesInShader();
    }

    private void SetObstaclesInShader()
    {
        Vector4[] obstaclePosition = new Vector4[64];
        float[] obstacleRadius = new float[64];

        for (int i = 0; i < 64; i++ )
        {
            if (i < obstacles.Count)//if obstacle, fill with its values
            {
                obstaclePosition[i] = obstacles[i].transform.position;
                obstacleRadius[i] = obstacles[i].Radius;
            }
            else//if no more obstacles, fill with negative radius
            {
                obstaclePosition[i] = Vector4.zero;
                obstacleRadius[i] = -1;
            }
        }

        //Set properties in shader
        Shader.SetGlobalVectorArray("_GrassObstaclePosition", obstaclePosition);
        Shader.SetGlobalFloatArray("_GrassObstacleRadius", obstacleRadius);
    }

    //Obstacles
    private static List<GrassObstacle> obstacles = new List<GrassObstacle>();

    public static void AddObstacle(GrassObstacle obs)
    {
        if (!obstacles.Contains(obs)) { obstacles.Add(obs); }
    }

    public static void RemoveObstacle(GrassObstacle obs)
    {
        obstacles.Remove(obs);
    }
}
