﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassObstacle : MonoBehaviour {

    public float Radius = 2.5f;

    void LateUpdate()
    {
        if (IsInCameraFrustum())
        {
            this.OnEnable();
        }
        else
        {
            this.OnDisable();
        }
    }


    void OnEnable()
    {
        if (IsInCameraFrustum())
        {
            InteractiveGrass.AddObstacle(this);
        }
    }

    void OnDisable()
    {
        InteractiveGrass.RemoveObstacle(this);
    }

    bool IsInCameraFrustum()
    {
        Camera cam = Camera.main;

        Vector3 viewPortPos = cam.WorldToViewportPoint(transform.position);

        return viewPortPos.x < 1 && viewPortPos.x > 0 && viewPortPos.y < 1 && viewPortPos.y > 0 && viewPortPos.z > 0;
    }
}
