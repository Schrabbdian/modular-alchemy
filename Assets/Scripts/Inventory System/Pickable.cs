﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine;

public class Pickable : MonoBehaviour, Interactable
{
    //UI stuff
    [SerializeField] private Sprite icon;

    public virtual void OnPickedUp()
    {
        gameObject.SetActive(false);
    }

    public virtual void OnDropped(Vector3 dropPosition)
    {
        transform.rotation = Quaternion.identity;

        gameObject.SetActive(true);
        transform.position = dropPosition;
    }


    public void OnInteract(Player player)
    {
        player.GetInventory().Add(this);
    }


    #region Getters & Setters

    public Sprite GetIcon()
    {
        return icon;
    }

    #endregion
}
