﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Storage : MonoBehaviour, Interactable 
{
    [SerializeField] private Inventory inventory;

    public float autoCloseDistance = 5.0f;//max distance the player can be away before storage closes automatically

    private Player interactPlayer = null;

    public void OnInteract(Player player)
    {
        if (inventory.IsOpen())
        {
            inventory.Close();
            interactPlayer = null;
        }
        else
        {
            inventory.Open();
            interactPlayer = player;
        }
    }

    void Update()
    {
        if (interactPlayer && Vector3.Distance(transform.position, interactPlayer.transform.position) > autoCloseDistance)
        {
            inventory.Close();
            interactPlayer = null;
        }
    }
}
