﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour 
{
    private Pickable item;

    //Component References
    private Animator animator;
    [SerializeField] private Image icon;
    [SerializeField] private Button button;

    private Inventory inventory = null;//the inventory this slot is associated with

    void Start()
    {
        //Set Component References
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        //Buttons should only be interactable when inventory open
        button.interactable = inventory.IsOpen();
    }


    public void DropItem(Vector3 dropPos)
    {
        item.OnDropped(dropPos);
        StartCoroutine(DeleteRoutine());
    }

    public void TransferItem()
    {
        List<Inventory> openInventories = Inventory.GetOpenInventories();

        Inventory destInventory = openInventories.Find(i => { return i != inventory; });

        if (destInventory != null)
        {
            TransferItem(destInventory);
        }
        else
        {
            //TODO dont hardcode
            DropItem(inventory.transform.position + inventory.transform.forward * 2.5f);
        }
    }

    public void TransferItem(Inventory newInventory)
    {
        bool transferred = newInventory.Add(this.item);

        if (transferred)
        {
            StartCoroutine(DeleteRoutine());
        }
    }

    private IEnumerator DeleteRoutine()
    {
        button.interactable = false;

        animator.SetTrigger("delete");
        inventory.Remove(this);

        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).IsName("deleted"));
        Destroy(gameObject);
    }

    #region Getters & Setters
    public void SetItem(Pickable item)
    {
        this.item = item;
        this.icon.sprite = item.GetIcon();
    }

    public Pickable GetItem()
    {
        return item;
    }

    public void SetInventory(Inventory inventory)
    {
        this.inventory = inventory;
    }

    public Inventory GetInventory()
    {
        return inventory;
    }
    #endregion

}
