﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class Inventory : MonoBehaviour 
{
    //Static Stuff
    private static List<Inventory> openInventories = new List<Inventory>();//ensures that only two inventories are open at a time

    //UI References
    [SerializeField] private Animator inventoryUIAnimator;
    [SerializeField] private LayoutGroup inventoryUIArea;
    [SerializeField] private ItemSlot slotPrefab;
    
    //Item memory
    [SerializeField]
    private int capacity = 10;
    private List<ItemSlot> itemSlots = new List<ItemSlot>();


    public bool Add(Pickable toAdd)
    {
        //If item can be added to inventory
        if (itemSlots.Count < capacity)
        {
            //Instantiate new Inventory Slot
            ItemSlot newSlot = Instantiate<ItemSlot>(slotPrefab);
            newSlot.transform.SetParent(inventoryUIArea.transform, false);
            newSlot.SetInventory(this);
            newSlot.SetItem(toAdd);

            //add new Slot to inventory
            itemSlots.Add(newSlot);

            //select newly created slot
            if (!IsOpen() || !EventSystem.current.currentSelectedGameObject) { EventSystem.current.SetSelectedGameObject(newSlot.gameObject); }

            //signal object that it was picked up
            toAdd.OnPickedUp();

            return true;
        }

        return false;
    }

    public bool Remove(ItemSlot toRemove)
    {
        int index = itemSlots.IndexOf(toRemove);

        bool removed = itemSlots.Remove(toRemove);

        //if we removed the selected
        if (removed && !EventSystem.current.currentSelectedGameObject)
        {
            if (itemSlots.Count > 0)//still items left in the inventory
            {
                //select the next item in this inventory instead
                EventSystem.current.SetSelectedGameObject(itemSlots[Mathf.Clamp(index, 0, itemSlots.Count - 1)].gameObject);
            }
            else//we deleted the last item in this inventory
            {
                Inventory openNotEmptyInv = openInventories.Find(i => { return i.itemSlots.Count > 0; });

                //select first item in other open, non-empty inventory
                if (openNotEmptyInv != null) { EventSystem.current.SetSelectedGameObject(openNotEmptyInv.itemSlots[0].gameObject); }
            }
        }

        return removed;

    }

    public void Open()
    {
        if (inventoryUIAnimator.IsInTransition(0)) return;

        inventoryUIAnimator.SetTrigger("Open");

         openInventories.Add(this);

        //if inventory contains items, select first upon opening
        if (!EventSystem.current.currentSelectedGameObject)
        {
            SelectFirstSlot();
        }


        //If more than two are now open
        if (openInventories.Count > 2)
        {
            //close the oldest one
            openInventories[0].Close();
        }
    }

    public void Close()
    {
        if (inventoryUIAnimator.IsInTransition(0)) return;

        inventoryUIAnimator.SetTrigger("Close");

        openInventories.Remove(this);

        //If another inventory is still open, select its first item
        GameObject selected = EventSystem.current.currentSelectedGameObject;
        ItemSlot selectedSlot = null;
        if (selected != null) { selectedSlot = selected.GetComponent<ItemSlot>(); }

        if(selectedSlot != null && selectedSlot.GetInventory() == this)

        if (openInventories.Count > 0)
        {
            openInventories[0].SelectFirstSlot();
        }
    }

    public void SelectFirstSlot()
    {
        if (itemSlots.Count > 0)
        {
            EventSystem.current.SetSelectedGameObject(itemSlots[0].gameObject);
        }
    }

    #region Getters & Setters
    public bool IsOpen()
    {
        return openInventories.Contains(this);
    }

    public static List<Inventory> GetOpenInventories()
    {
        return openInventories;
    }

    #endregion
}
