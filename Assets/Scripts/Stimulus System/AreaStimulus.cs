﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StimulusSystem;

namespace StimulusSystem
{
    /// <summary>
    /// Sends out a stimulus in a circular area around the object.
    /// </summary>
    public abstract class AreaStimulus : StimulusProvider
    {
        [Header("Stimulus Area:")]
        public float Radius;
        public AnimationCurve distanceFalloff;

        public LayerMask layerMaskToStimulate;

        public bool canBeBlocked = false;
        public LayerMask layerMaskObstacle;


        protected virtual void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(transform.position, Radius);
        }

        protected override void SendStimulus()
        {
            //Get All Objects in Radius
            Collider[] hitObjects = Physics.OverlapSphere(transform.position, Radius, layerMaskToStimulate.value);

            //For every Object in the Radius, collect its Receiver Scripts
            foreach (Collider c in hitObjects)
            {
                Vector3 d = c.transform.position - transform.position;
                RaycastHit hit = new RaycastHit();
                if (canBeBlocked) { Physics.Raycast(transform.position, d.normalized, out hit, d.magnitude, layerMaskObstacle.value); }


                //make sure stimulus isnt blocked
                if (!canBeBlocked || hit.collider == null || hit.collider.Equals(c))
                {
                    Receiver[] receiverScripts = c.GetComponents<Receiver>();

                    //Modify stimulus intensity based on distance
                    PotionSystem.Effect stimulus = new PotionSystem.Effect(this.effect);
                    stimulus.intensity = distanceFalloff.Evaluate(d.magnitude / Radius) * stimulus.intensity;

                    //send stimulus to all receiver scripts
                    foreach (Receiver r in receiverScripts)
                    {
                        r.ReactTo(stimulus);
                    }
                }
            }
        }
    }
}
