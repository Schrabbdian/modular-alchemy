﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace StimulusSystem
{
    /// <summary>
    /// Base class for everything that can send out a Stimulus.
    /// </summary>
    public abstract class StimulusProvider : MonoBehaviour
    {
        [Header("Type of Stimulus:")]
        public PotionSystem.Effect effect;

        protected abstract void SendStimulus();
    }
}
