﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StimulusSystem;
using PotionSystem;

/// <summary>
/// Receives Heat Stimuli and calculates a Temperature from them.
/// </summary>
public class Temperature : MonoBehaviour, Receiver
{
    [Header("Temperature Change:")]
    public float heatSpeed = 0.1f;
    public float coolSpeed = 0.05f;

    private float currentTemperature = 0.0f;

    private void Update()
    {
        //Make temperature Decay to 0.0
        float lerpSpeed = (currentTemperature < 0) ? heatSpeed : coolSpeed;
        currentTemperature = Mathf.Lerp(currentTemperature, 0.0f, 10.0f * lerpSpeed * Time.deltaTime);
    }

    void Receiver.ReactTo(Effect stimulus)
    {
        if (stimulus.type != Effect.Type.Fire) return;//only react to fire stimulus

        //Change temperature based on stimulus
        if (stimulus.intensity > 0.0f)
        {
            currentTemperature += heatSpeed * stimulus.intensity;
        }
        else
        {
            currentTemperature += coolSpeed * stimulus.intensity;
        }

        //ensure temperature stays reasonable (within [-1.0f, 1.0f])
        currentTemperature = Mathf.Clamp(currentTemperature, -1.0f, 1.0f);
    }

    public float GetCurrent()
    {
        return currentTemperature;
    }
}
