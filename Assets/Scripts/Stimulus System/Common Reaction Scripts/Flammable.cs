﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StimulusSystem;
using PotionSystem;

/// <summary>
/// Makes an object burn if its Temperature is high enough for a while.
/// Once burning, the object will send out an Area Stimulus at an interval.
/// </summary>
[RequireComponent(typeof(Temperature))]
public class Flammable : AreaStimulus
{
    [Header("Stimulus Timing:")]
    public float Interval;

    [Header("Ignition Parameters:")]
    public float heatThreshold = 0.5f;
    public float ignitionSpeed = 5.0f;

    [Header("Visuals:")]
    public GameObject FlameEffect;

    private bool isBurning = false;
    private GameObject fireEffect;

    //Component References
    protected Temperature temperature;

    //Timers
    private float ignitionTimer = 0.0f;
    protected float burnTimer = 0.0f;
    private float stimulusTimer = 0.0f;


    protected virtual void Start()
    {
        //Set Component References
        temperature = GetComponent<Temperature>();
    }

    protected virtual void Update()
    {
        if (isBurning)
        {
            // only send out stimulus while burning
            
            if (stimulusTimer <= 0.0f)//Send Out a Stimulus every %Interval% seconds
            {
                SendStimulus();
                stimulusTimer = Interval;
            }

            stimulusTimer -= Time.deltaTime;
            burnTimer += Time.deltaTime;
        }
        else//not yet burning
        {
            float t = temperature.GetCurrent();

            //If hot enough, increase ignition timer
            if (t > heatThreshold)
            {
                ignitionTimer = Mathf.Lerp(ignitionTimer, 1.1f, ignitionSpeed * Time.deltaTime);
            }
            else//otherwise make it decay
            {
                ignitionTimer = Mathf.Lerp(ignitionTimer, 0.0f, ignitionSpeed * Time.deltaTime);
            }


            //If timer high enough, start Burning
            if (ignitionTimer > 1.0f) { Ignite(); }
        }
    }


    public void Ignite()
    {
        if (isBurning) return;

        isBurning = true;
        fireEffect = Instantiate(FlameEffect, this.transform.position, this.transform.rotation, this.transform);
    }

    public void Extinguish()
    {
        if (!isBurning) return;

        isBurning = false;
        Destroy(fireEffect);
        fireEffect = null;
    }

    public bool IsBurning()
    {
        return isBurning;
    }
}
