﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PotionSystem;

namespace StimulusSystem
{
    /// <summary>
    /// Interface which needs to be implemented by any object script which should react to stimulus of some kind.
    /// </summary>
    public interface Receiver
    {
        void ReactTo(Effect stimulus);
    }
}
