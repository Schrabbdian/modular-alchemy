﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour 
{
    [Header("Ground Movement")]
    [SerializeField] float TurnSpeed;
    [SerializeField] AnimationCurve TurnSpeedByForwardSpeed;

    [Header("Jumping")]
    [SerializeField] float jumpStrength;
    [SerializeField] float gravityMultiplier;
    [SerializeField] float airControl;
    [SerializeField] float maxAirSpeed;
    [SerializeField] float turnSpeedAir;

    [Header("Ground Collision")]
    [SerializeField] float groundCheckDistance = 0.1f;
    [SerializeField] LayerMask groundMask;

    [Header("Object Interaction")]
    [SerializeField] Inventory inventory;

    //Component References
    Rigidbody rigidBody;
    Animator animator;
    


    //Movement
    float turnAmount;
    float forwardAmount;

    //Collision / Ground
    Vector3 groundNormal;
    bool isGrounded;
    float original_groundCheckDistance;


    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        rigidBody = GetComponent<Rigidbody>();


        rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
        original_groundCheckDistance = groundCheckDistance;
    }

    public void Move(Vector3 move, bool jump, bool interact, bool inventoryInput)
    {
        // convert the world relative moveInput vector into a local-relative
        // turn amount and forward amount required to head in the desired
        // direction.
        if (move.magnitude > 1f) move.Normalize();
        Vector3 moveWorld = move;
        move = transform.InverseTransformDirection(move);

        CheckGroundStatus();

        move = Vector3.ProjectOnPlane(move, groundNormal);
        turnAmount = Mathf.Atan2(move.x, move.z);
        forwardAmount = move.z;
        
        // control and velocity handling is different when grounded and airborne:
        if (isGrounded)
        {
            HandleGroundedMovement(jump);
        }
        else
        {
            HandleAirborneMovement(moveWorld);
        }

        //Turn into movement Direction
        Turn();

        #region Item Management / Interaction
        //Interact with things when button pressed
        if (interact)
        {
            HandleInteractions();
        }

        //Open / Close Inventory (if there is one)
        if (inventoryInput && inventory)
        {
            if (inventory.IsOpen())
            {
                inventory.Close();
            }
            else
            {
                inventory.Open();
            }
        }

        #endregion


        // send input and other state parameters to the animator
        UpdateAnimator(move);
    }

    void UpdateAnimator(Vector3 move)
    {
        // update the animator parameters
        animator.SetFloat("Forward", forwardAmount, 0.1f, Time.deltaTime);
        animator.SetFloat("Turn", turnAmount, 0.1f, Time.deltaTime);
        animator.SetBool("OnGround", isGrounded);
        
        if (!isGrounded)
        {
            animator.SetFloat("Jump", rigidBody.velocity.y);
        }
    }

    public void OnAnimatorMove()
    {
        if (isGrounded && Time.deltaTime > 0)
        {
            Vector3 v = animator.deltaPosition / Time.deltaTime;
            // we preserve the existing y part of the current velocity.
            v.y = rigidBody.velocity.y;
        
            rigidBody.velocity = v;
        }
    }

    void HandleGroundedMovement(bool jump)
    {
        // check whether conditions are right to allow a jump:
        if (jump && (animator.GetCurrentAnimatorStateInfo(0).IsName("GroundMovement")))
        {
            // jump up, super star!
            rigidBody.velocity = new Vector3(rigidBody.velocity.x, jumpStrength, rigidBody.velocity.z);
            isGrounded = false;
            animator.applyRootMotion = false;
            groundCheckDistance = 0.01f;
        }
    }

    void HandleAirborneMovement(Vector3 move)
    {
        // apply extra gravity from multiplier:
        Vector3 extraGravityForce = (Physics.gravity * gravityMultiplier) - Physics.gravity;
        rigidBody.AddForce(extraGravityForce * rigidBody.mass);

        groundCheckDistance = rigidBody.velocity.y < 0 ? original_groundCheckDistance : 0.01f;

        //Enable control over velocity while in air
        Vector3 velDelta = move * airControl * Time.deltaTime;
        Vector3 velAir = rigidBody.velocity + velDelta;
        velAir.y = 0.0f;
        velAir = Vector3.ClampMagnitude(velAir, maxAirSpeed);

        rigidBody.velocity = new Vector3(velAir.x, rigidBody.velocity.y, velAir.z);
    }

    void Turn()
    {
        //Turning
        float speed = TurnSpeedByForwardSpeed.Evaluate(forwardAmount);
        speed *= isGrounded ? TurnSpeed : turnSpeedAir;

        //Turn into walking direction
        rigidBody.MoveRotation(rigidBody.rotation * Quaternion.Euler(transform.up * turnAmount * speed * Time.deltaTime));

    }

    void HandleInteractions()
    {
        if (!isGrounded) return; // only interact when on ground

        //Spherecast in viewing direction
        Vector3 rayOrigin = transform.position + transform.up * transform.localScale.y;
        Vector3 rayDir = transform.forward;

        Debug.DrawRay(rayOrigin, rayDir * 1.0f, Color.red, Time.deltaTime, false);

        RaycastHit hit = new RaycastHit();
        bool hitSomething = Physics.SphereCast(rayOrigin, 1.0f, rayDir, out hit, 1.0f);

        if (!hitSomething) return;//if nothing was hit, stop here

        //Get interactable scripts on the object
        Interactable[] interactables = hit.transform.GetComponents<Interactable>();

        foreach (Interactable i in interactables)
        {
            //call their interaction functions
            i.OnInteract(this);
        }
    }

    void CheckGroundStatus()
    {
        RaycastHit hitInfo;
        #if UNITY_EDITOR
        // helper to visualise the ground check ray in the scene view
        Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * groundCheckDistance));
        #endif
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, groundCheckDistance))
        {
            groundNormal = hitInfo.normal;
            isGrounded = true;
            animator.applyRootMotion = true;
        }
        else
        {
            isGrounded = false;
            groundNormal = Vector3.up;
            animator.applyRootMotion = false;
        }
    }


    #region Getters & Setters
    public Inventory GetInventory()
    {
        return inventory;
    }
    #endregion
}
