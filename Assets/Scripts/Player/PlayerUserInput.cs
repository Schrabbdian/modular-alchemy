﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(Player))]
public class PlayerUserInput : MonoBehaviour 
{
    [Header("Enabled Inputs")]
    [SerializeField] private bool enableMove = true;
    [SerializeField] private bool enableJump = true;
    [SerializeField] private bool enableInteract = true;
    [SerializeField] private bool enableInventory = true;

    private Player player; // A reference to the ThirdPersonCharacter on the object
    private Transform cam;                  // A reference to the main camera in the scenes transform
    private Vector3 camForward;             // The current forward direction of the camera

    private Vector3 move;                   // the world-relative desired move direction, calculated from the camForward and user input.
    private bool jumpInput;
    private bool interactInput;
    private bool inventoryInput;

    private void Start()
    {
        // get the transform of the main camera
        if (Camera.main != null)
        {
            cam = Camera.main.transform;
        }
        else
        {
            Debug.LogWarning(
                "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
            // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
        }

        // get the third person character ( this should never be null due to require component )
        player = GetComponent<Player>();
    }


    private void Update()
    {
        if (enableJump &&!jumpInput)
        {
            jumpInput = CrossPlatformInputManager.GetButtonDown("Jump");
        }

        if (enableInteract && !interactInput)
        {
            interactInput = CrossPlatformInputManager.GetButtonDown("Interact");
        }

        if (enableInventory && !inventoryInput)
        {
            inventoryInput = CrossPlatformInputManager.GetButtonDown("Inventory");
        }
    }


    // Fixed update is called in sync with physics
    private void FixedUpdate()
    {
        // read inputs
        float h = 0.0f; 
        float v = 0.0f;
        if (enableMove)
        {
            h = CrossPlatformInputManager.GetAxis("Horizontal");
            v = CrossPlatformInputManager.GetAxis("Vertical");
        } 

        // calculate move direction to pass to character
        if (cam != null)
        {
            // calculate camera relative direction to move:
            camForward = Vector3.Scale(cam.forward, new Vector3(1, 0, 1)).normalized;
            move = v * camForward + h * cam.right;
        }
        else
        {
            // we use world-relative directions in the case of no main camera
            move = v * Vector3.forward + h * Vector3.right;
        }

        // pass all parameters to the character control script
        player.Move(move, jumpInput, interactInput, inventoryInput);
        jumpInput = false;
        interactInput = false;
        inventoryInput = false;
    }

}
