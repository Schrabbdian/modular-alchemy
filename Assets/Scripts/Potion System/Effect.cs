﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PotionSystem
{
    /// <summary>
    /// Kinds of effects a potion can have. Also doubles as types of Stimulus that can be sent out.
    /// </summary>
    [System.Serializable]
    public struct Effect
    {
        public enum Type
        {
            Growth,
            Fire
        }

        public float intensity;
        public Type type;

        //Copy Constructor
        public Effect(Effect toCopy)
        {
            this.intensity = toCopy.intensity;
            this.type = toCopy.type;
        }
    }
}